// get-image-slimer.js

// Arkanon <arkanon@lsd.org.br>
// 2020/06/12 (Fri) 22:55:16 -03

/*

  # <http://code-epicenter.com/how-to-login-to-facebook-using-casperjs>
  # <http://subscription.packtpub.com/book/web_development/9781783981922/1/ch01lvl1sec14/running-phantomjs-with-a-disk-cache>
  # <http://code.tutsplus.com/tutorials/responsive-screenshots-with-casper--net-33142>

  alias casperjs='time casperjs --disk-cache=true --cookies-file=$HOME/.cache/fbjs-cookies.txt'

  l ~/'.cache/Ofi Labs/PhantomJS/data8'

*/



   var _left     =   41
   var _top      =   23
   var _width    = 1440
   var _height   =  810
   var _tab      =  360
   var _selector = 'div[data-testid="post_message"] p'



   var fs   = require('fs')
// var util = require('utils')
   var sys  = require('system')
   var page = require("webpage").create
   (
     {
       verbose      : false,
       logLevel     : "debug",
    // waitTimeout  : 60*1000, // msec
    // stepTimeout  : 10*1000,
       viewportSize : {
                        width  : _width  + 2*_left + _tab,
                        height : _height + 2*_top
                      },
       pageSettings : {
                        userAgent          : "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:77.0) Gecko/20100101 Firefox/77.0", // indicar um navegador desktop força a versão web do login
                        loadImages         : true,
                        loadPlugins        : false,
                        webSecurityEnabled : false,
                        ignoreSslErrors    : true
                      }
     }
   )



// var fbid = page.cli.args[0]
   var fbid = 2965057990229505



   page.open(
   (
     "http://fb.com/photo.php?fbid=" + fbid,
     function()
     {
       console.log(fbid)
     }
   )



   page.then
   (
     function()
     {
       var caption = this.getElementsInfo(_selector)[0].html
       console.log(caption)
       fs.write( fbid + ".txt", caption + "\n", "w" )
     }
   )



   page.then
   (
     function()
     {

       this.capture
       (
         fbid + ".png",
         {
           left   : _left,
           top    : _top,
           width  : _width,
           height : _height
         }
       )

    /*
       this.capture
       (
         "images/" + fbid + "-info.png",
         {
           left   : _left + _width,
           top    : _top,
           width  : _tab,
           height : _height
         }
       )
    */

     }
   )



   page.run()



// EOF

// facebook-login-slimer.js

// Arkanon <arkanon@lsd.org.br>
// 2020/06/18 (Thu) 01:59:21 -03
// 2020/06/12 (Fri) 22:55:16 -03

// <https://docs.slimerjs.org/current/quick-start.html>

//  | grep -oP '<(form|(input|button)[^>]*type="(text|password|submit)")[^>]*>'



   var ss  = "afterlogin.png"
// var url = "http://fb.com" // sem UA indicando navegador desktop, direciona para a versão mobile do FB, com uma página extra oferecendo entrada com um toque
   var url = "https://www.facebook.com/login"



   var fs   = require('fs')
// var util = require('utils')
   var sys  = require('system')
   var page = require('webpage').create
   (
     {
       verbose      : false,
       logLevel     : 'debug',
    // waitTimeout  : 10*1000, // msec
    // stepTimeout  : 10*1000,
       viewportSize : {
                        width  : 1000,
                        height :  600
                      },
       pageSettings : {
                        userAgent          : "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:77.0) Gecko/20100101 Firefox/77.0", // indicar um navegador desktop força a versão web do login
                        loadImages         : false,
                        loadPlugins        : false,
                        webSecurityEnabled : false,
                        ignoreSslErrors    : true
                      }
     }
   )



   var user = sys.env.USER.replace(/\r?\n?/gm, "")
   var pass = fs.read( sys.env.ADM_DIR + "/.f" ).replace(/\r?\n?/gm, "")
// console.log( "[" + user + "]:[" + pass + "]" )



   page.open
   (
     url,
     function(status)
     {
       var body = page.evaluate( function() { return document.body.innerHTML } )
    // console.log( body )
       page.evaluate
       (
         function(user, pass)
         {
           document.querySelector('[name="email"]').value = user
           document.querySelector('[name="pass"]' ).value = pass
           document.querySelector('[name="login"]').click()
         },
         user,
         pass
       )
    // page.render(ss)
       slimer.exit()
     }
   )



// EOF

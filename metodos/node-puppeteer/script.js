// script.js

// Arkanon <arkanon@lsd.org.br>
// 2023/04/29 (Sat) 12:25:20 -03

// <http://github.com/zoutepopcorn/pup-face/blob/master/index.js>
// <http://stackoverflow.com/a/24638042> How to pass command line arguments to a Node.js program



const url  = 'http://fb.com';
const ss   = '/host/fb.png';

const pupp = require('puppeteer');
const argv = require('minimist')(process.argv.slice(2));
const CRED = require(argv.creds);

// console.dir(argv);
// console.dir(argv.creds);
// console.dir(CRED.user);



const sleep = async (ms) => {
  return new Promise((res, rej) => {
    setTimeout(() => {
      res();
    }, ms)
  });
}



const ID = {
  login: '#email',
  pass: '#pass'
};



(
  async () => {

    const browser = await pupp.launch({
      headless: 'new', // http://developer.chrome.com/articles/new-headless
      args: ['--no-sandbox', '--disable-setuid-sandbox']
    });

    const page = await browser.newPage();

    let  login = async () => {

      await page.goto(url, { waitUntil: 'networkidle2' });

      await page.waitForSelector(ID.login);
      console.log(CRED.user);
      console.log(ID.login);

      await page.type(ID.login, CRED.user);
      await page.type(ID.pass, CRED.pass);
      await sleep(500);

      await page.keyboard.press('Enter');

      console.log("login done");
      await page.waitForNavigation();

    }

    await login();
    await page.screenshot({ path: ss });
    await browser.close();

})();



// EOF

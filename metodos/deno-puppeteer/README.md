# Instalação do Deno em container

- http://deno.com/manual/getting_started/installation
- http://hub.docker.com/r/denoland/deno
- http://deno.com/manual/examples
- http://examples.deno.land
- http://deno.land/x/puppeteer



```bash
  image=denoland/deno
  deno(){ docker run "${args[@]}" "$@"; }
# export PUPPETEER_PRODUCT=chromium
  export PUPPETEER_PRODUCT=chrome
# export PUPPETEER_PRODUCT=firefox
```



```bash
  ((UID>0)) && hash sudo 2>&- && sudo="sudo -E"; echo $sudo

  $sudo apt -y update
  apt list --upgradable
  $sudo apt -y install docker-ce
  $sudo apt -y upgrade

  docker --version

  docker pull $image
  docker images
```



## Container interativo

```bash
  cname=deno-puppeteer

  args=(
    --detach
    --interactive
    --tty
    --init
    --entrypoint bash
    --env        TZ=$(</etc/timezone)
    --publish    1993:1993
    --volume     $PWD:/app
    --workdir    /app
    --env        PUPPETEER_PRODUCT=$PUPPETEER_PRODUCT
    --hostname   $cname
    --name       $cname
    $image
  )
  declare -p args | tr ' ' '\n'
  docker rm -f $cname
  docker run "${args[@]}"

  docker ps -a

  docker exec -ti $cname bash


  apt update
  apt install -y wget gnupg
  wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
  echo deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main > /etc/apt/sources.list.d/google.list
  apt update
  apt list --upgradable
  apt upgrade -y
  apt install -y google-chrome-stable fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-kacst fonts-freefont-ttf libxss1 --no-install-recommends
  rm -rf /var/lib/apt/lists/*
```



## Container não interativo

```bash
  args=(
    --interactive
    --rm
    --publish 1993:1993
    --volume  $PWD:/app
    --workdir /app
    --env     PUPPETEER_PRODUCT=$PUPPETEER_PRODUCT
    --env     WEB_RES_SCREENSHOT_BASE_PATH=$WEB_RES_SCREENSHOT_BASE_PATH
    $image
  )
  declare -p args | tr ' ' '\n'

  deno run --allow-net - << EOT
console.log('Hello, World!')
EOT
```



## Teste

https://github.com/puppeteer/puppeteer/blob/main/docs/troubleshooting.md

```bash

  export WEB_RES_SCREENSHOT_BASE_PATH=./screenshots

  time deno run --unstable -A mod.ts http://fb.com

  time deno run --unstable -A - << EOT
// import                "https://deno.land/x/puppeteer/install.ts"
EOT
# 1m28.503s

  time deno run --unstable -A - << EOT

   import puppeteer from "https://deno.land/x/puppeteer/mod.ts";

   const browser = await puppeteer.launch(
   {
      executablePath: '/usr/bin/google-chrome-stable',
      ignoreDefaultArgs: [
         '--disable-extensions'
      ],
      headless: true,
      args: [
         '--use-gl=egl',
         '--no-sandbox',
         '--disable-setuid-sandbox'
      ],
   });

   const page = await browser.newPage();

   await page.goto("https://fb.com");

   await page.screenshot(
   {
      path: "fb.png",
   });

   await browser.close();

EOT

```



☐

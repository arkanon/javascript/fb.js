// get-image-mobile.js

// Arkanon <arkanon@lsd.org.br>
// 2020/06/19 (Fri) 15:33:12 -03
// 2020/06/19 (Fri) 03:39:39 -03



   var fs     = require("fs")
   var sys    = require("system")
   var casper = require("casper")



   var page = casper.create
   (
     {
       verbose      : false,
       logLevel     : "debug",
       pageSettings : {
                        loadImages         : false,
                        loadPlugins        : false,
                        webSecurityEnabled : false,
                        ignoreSslErrors    : true
                      }
     }
   )



   var script = sys.args[sys.args.length-1]
   var fbid   = page.cli.args[0]

   if (fbid===undefined)
   {
     console.log("\nUsage: casperjs [--disk-cache=true] --cookies-file=<cookies_file> " + script + " <image_fbid>\n")
     page.exit()
   }



   page.start().thenOpen
   (
     "http://m.facebook.com/photo/view_full_size/?fbid=" + fbid,
     function()
     {
       console.log( fbid )
       var html  = this.evaluate( function() { return document.all[0].outerHTML } )
       var image = this.getElementsInfo("img")[0].attributes["src"]
       console.log( image )
       fs.write( fbid + "-photo.html", html + "\n", "w" )
       this.download( image, fbid + ".jpg" );
     }
   ).thenOpen
   (

     // 3174329112635724 # 71
     // 3163927447009224 # 69
     // 3125179100884059 # 58
     // 3064364576965512 # 38
     // 3021490974586206 # 23
     // 2965057990229505 # 01
     "http://m.facebook.com/photo?fbid=" + fbid,

     function()
     {
       var html    = this.evaluate( function() { return document.all[0].outerHTML } )
       var caption = this.getElementsInfo('div[class="msg"] div')[0].html
       console.log( caption )
       fs.write( fbid + ".html" , html    + "\n" , "w" )
       fs.write( fbid + ".txt"  , caption + "\n" , "w" )
     }

   ).run()



// EOF

// get-image-screenshot.js

// Arkanon <arkanon@lsd.org.br>
// 2020/06/19 (Fri) 03:37:51 -03


   var _left    =   41
   var _top     =   23
   var _width   = 1440
   var _height  =  810
   var _tab     =  360
   var _caption = 'div[data-testid="post_message"] p'



   var fs     = require("fs")
// var util   = require("utils")
   var sys    = require("system")
   var casper = require("casper")



   var page = casper.create
   (
     {
       verbose      : false,
       logLevel     : "debug",
    // waitTimeout  : 60*1000, // msec
    // stepTimeout  : 10*1000,
       viewportSize : {
                        width  : _width  + 2*_left + _tab,
                        height : _height + 2*_top
                      },
       pageSettings : {
                        loadImages         : true,
                        loadPlugins        : false,
                        webSecurityEnabled : false,
                        ignoreSslErrors    : true
                      }
     }
   )


   var script = sys.args[sys.args.length-1]
   var wd     = fs.workingDirectory
   var fbid   = page.cli.args[0]
   var url    = "http://fb.com/photo?fbid=" + fbid



   page.start()



   page.then
   (
     function()
     {
       if (fbid===undefined)
       {
         console.log("\nUsage: casperjs [--disk-cache=true] --cookies-file=<cookies_file> " + script + " <image_fbid>\n")
         this.exit()
       }
     }
   )



   page.thenOpen(url)



   page.then
   (
     function()
     {

       var html    = this.evaluate( function() { return document.all[0].outerHTML } )
    // var html    = this.getElementsInfo('html'  )[0].html // retorna apenas o conteudo da tag html (html.innerHTML), sem a tag html propriamente dita
       var caption = this.getElementsInfo(_caption)[0].html
       console.log( fbid + '\n' + caption )
       fs.write( fbid + ".html" , html    + "\n" , "w" )
       fs.write( fbid + ".txt"  , caption + "\n" , "w" )

       this.capture
       (
         fbid + "-ss.png"
       )

       this.capture
       (
         fbid + ".png",
         {
           left   : _left,
           top    : _top,
           width  : _width,
           height : _height
         }
       )

       this.capture
       (
         fbid + "-caption.png",
         {
           left   : _left + _width,
           top    : _top,
           width  : _tab,
           height : _height
         }
       )

     }
   )



   page.run()



// EOF

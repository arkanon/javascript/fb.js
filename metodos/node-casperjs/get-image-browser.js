// get-image-browser.js

// Arkanon <arkanon@lsd.org.br>
// 2020/06/19 (Fri) 03:37:25 -03



   var _caption = 'div[data-testid="post_message"] p'
   var _image   = 'a[rel="theater"]'



   var fs     = require("fs")
   var sys    = require("system")
   var casper = require("casper")



   var page = casper.create
   (
     {
       verbose      : false,
       logLevel     : "debug",
       pageSettings : {
                        loadImages         : false,
                        loadPlugins        : false,
                        webSecurityEnabled : false,
                        ignoreSslErrors    : true
                      }
     }
   )


   var script = sys.args[sys.args.length-1]
   var wd     = fs.workingDirectory
   var fbid   = page.cli.args[0]
   var url    = "http://fb.com/photo?fbid=" + fbid



   page.start()



   page.then
   (
     function()
     {
       if (fbid===undefined)
       {
         console.log("\nUsage: casperjs [--disk-cache=true] --cookies-file=<cookies_file> " + script + " <image_fbid>\n")
         this.exit()
       }
     }
   )



   page.thenOpen(url)



   page.then
   (
     function()
     {
       var html    = this.evaluate( function() { return document.all[0].outerHTML } )
       var caption = this.getElementsInfo(_caption)[0].html
       var image   = this.getElementsInfo(_image  )[1].attributes["data-ploi"] // ver [análise] abaixo
       var json    = JSON.stringify(this.getElementsInfo(_image)) // retorna o objeto json em forma de string que pode ser usada para [análise]
       console.log( fbid + '\n' + image + '\n' + caption )
       fs.write( fbid + ".html" , html    + "\n" , "w" )
       fs.write( fbid + ".txt"  , caption + "\n" , "w" )
       this.download( image, fbid + ".jpg" )
     }
   )



   page.run()



/*

  >> análise do código html que disponibiliza a imagem  <<

   <A

    AJAXIFY              = "https://www.facebook.com/photo.php?fbid=3074488082619828&amp;set=a.159724900762842&amp;type=3&amp;eid=ARB3x3BjJ8_mCTSBS9R1pWigNBiBvoryW6OWPHQmQlyWvMOcifpXqA_sVf5GFKwLCvqP_2Pl_z5Hy-3O&amp;size=1440%2C810&amp;source=13&amp;player_origin=photos"
    CLASS                = "_4-eo _2t9n"
    DATA-PLOI            = "https://scontent.ffln4-1.fna.fbcdn.net/v/t1.0-9/96083540_3074488085953161_3597740661857583104_o.jpg?_nc_cat=110&amp;_nc_sid=110474&amp;_nc_ohc=oDyvLJ6Io9cAX-dc4kd&amp;_nc_ht=scontent.ffln4-1.fna&amp;oh=b8b0821c1b33d233c6070e89a542596f&amp;oe=5F100C9B"
    DATA-PLSI            = "https://scontent.ffln4-1.fna.fbcdn.net/v/t1.0-0/p180x540/96083540_3074488085953161_3597740661857583104_o.jpg?_nc_cat=110&amp;_nc_sid=110474&amp;_nc_ohc=oDyvLJ6Io9cAX-dc4kd&amp;_nc_ht=scontent.ffln4-1.fna&amp;_nc_tp=6&amp;oh=28137b291a007224db8cdf067eafb4f8&amp;oe=5F11A58E"
    DATA-RENDER-LOCATION = "permalink"
    HREF                 = "https://www.facebook.com/photo.php?fbid=3074488082619828&amp;set=a.159724900762842&amp;type=3&amp;eid=ARB3x3BjJ8_mCTSBS9R1pWigNBiBvoryW6OWPHQmQlyWvMOcifpXqA_sVf5GFKwLCvqP_2Pl_z5Hy-3O"
    REL                  = "theater"
    STYLE                = "width:500px;"

   >

   page.getElementsInfo(_image) =
   [

     {
       "attributes" :
       {
         "ajaxify"  : "https://www.facebook.com/photo.php?fbid=3169130259822276&set=a.159724900762842&type=3&size=1440%2C810&source=12&player_origin=photos"
       , "class"    : "_5pcq"
       , "href"     : "https://www.facebook.com/photo.php?fbid=3169130259822276&set=a.159724900762842&type=3"
       , "rel"      : "theater"
       , "target"   : ""
       }
     , "height"   : 15
     , "html"     : "<abbr data-utime=\"1592339677\" title=\"Terça-feira, 16 de junho de 2020 às 17:34\" data-shorten=\"1\" class=\"_5ptz\"><span class=\"timestampContent\" id=\"js_1\">16 de junho às 17:34</span></abbr>"
     , "nodeName" : "a"
     , "tag"      : "<a rel=\"theater\" ajaxify=\"https://www.facebook.com/photo.php?fbid=3169130259822276&amp;set=a.159724900762842&amp;type=3&amp;size=1440%2C810&amp;source=12&amp;player_origin=photos\" class=\"_5pcq\" href=\"https://www.facebook.com/photo.php?fbid=3169130259822276&amp;set=a.159724900762842&amp;type=3\" target=\"\"><abbr data-utime=\"1592339677\" title=\"Terça-feira, 16 de junho de 2020 às 17:34\" data-shorten=\"1\" class=\"_5ptz\"><span class=\"timestampContent\" id=\"js_1\">16 de junho às 17:34</span></abbr></a>"
     , "text"     : "16 de junho às 17:34"
     , "visible"  : true
     , "width"    : 112.03125
     , "x"        : 687
     , "y"        : 140
     }

   , {
       "attributes" :
       {
         "ajaxify"              : "https://www.facebook.com/photo.php?fbid=3169130259822276&set=a.159724900762842&type=3&eid=ARBgbaoU95dsrTfFpLL_IKjGdgUqL6qeevvjrqXm8iZhxjUUYt8jKU6sZr3eJEmqBfN2EXFV6_gVaXVr&size=1440%2C810&source=13&player_origin=photos"
       , "class"                : "_4-eo _2t9n"
       , "data-ploi"            : "https://scontent.ffln4-1.fna.fbcdn.net/v/t1.0-9/82957482_3169130263155609_7514841459699383389_o.jpg?_nc_cat=100&_nc_sid=110474&_nc_ohc=b2ur-L_GUA4AX_uRRa7&_nc_ht=scontent.ffln4-1.fna&oh=569b0c8442509caf255ccfb4f1827371&oe=5F12EE1C"
       , "data-plsi"            : "https://scontent.ffln4-1.fna.fbcdn.net/v/t1.0-0/p180x540/82957482_3169130263155609_7514841459699383389_o.jpg?_nc_cat=100&_nc_sid=110474&_nc_ohc=b2ur-L_GUA4AX_uRRa7&_nc_ht=scontent.ffln4-1.fna&_nc_tp=6&oh=743e12e8b95bb9815532d071b28b7a47&oe=5F13928D"
       , "data-render-location" : "permalink"
       , "href"                 : "https://www.facebook.com/photo.php?fbid=3169130259822276&set=a.159724900762842&type=3&eid=ARBgbaoU95dsrTfFpLL_IKjGdgUqL6qeevvjrqXm8iZhxjUUYt8jKU6sZr3eJEmqBfN2EXFV6_gVaXVr"
       , "rel"                  : "theater"
       , "style"                : "width:500px;"
       }
     , "height"   : 281
     , "html"     : "<div class=\"uiScaledImageContainer _4-ep\" style=\"width:500px;height:281px;\" id=\"u_0_12\"><img class=\"scaledImageFitWidth img\" src=\"https://scontent.ffln4-1.fna.fbcdn.net/v/t1.0-0/p526x296/82957482_3169130263155609_7514841459699383389_o.jpg?_nc_cat=100&amp;_nc_sid=110474&amp;_nc_ohc=b2ur-L_GUA4AX_uRRa7&amp;_nc_ht=scontent.ffln4-1.fna&amp;_nc_tp=6&amp;oh=407f2551636b4039902750f373019613&amp;oe=5F111A46\" data-src=\"https://scontent.ffln4-1.fna.fbcdn.net/v/t1.0-0/p526x296/82957482_3169130263155609_7514841459699383389_o.jpg?_nc_cat=100&amp;_nc_sid=110474&amp;_nc_ohc=b2ur-L_GUA4AX_uRRa7&amp;_nc_ht=scontent.ffln4-1.fna&amp;_nc_tp=6&amp;oh=407f2551636b4039902750f373019613&amp;oe=5F111A46\" alt=\"A imagem pode conter: céu, nuvem, crepúsculo e atividades ao ar livre\" width=\"500\" height=\"282\"></div>"
     , "nodeName" : "a"
     , "tag"      : "<a rel=\"theater\" ajaxify=\"https://www.facebook.com/photo.php?fbid=3169130259822276&amp;set=a.159724900762842&amp;type=3&amp;eid=ARBgbaoU95dsrTfFpLL_IKjGdgUqL6qeevvjrqXm8iZhxjUUYt8jKU6sZr3eJEmqBfN2EXFV6_gVaXVr&amp;size=1440%2C810&amp;source=13&amp;player_origin=photos\" data-ploi=\"https://scontent.ffln4-1.fna.fbcdn.net/v/t1.0-9/82957482_3169130263155609_7514841459699383389_o.jpg?_nc_cat=100&amp;_nc_sid=110474&amp;_nc_ohc=b2ur-L_GUA4AX_uRRa7&amp;_nc_ht=scontent.ffln4-1.fna&amp;oh=569b0c8442509caf255ccfb4f1827371&amp;oe=5F12EE1C\" data-plsi=\"https://scontent.ffln4-1.fna.fbcdn.net/v/t1.0-0/p180x540/82957482_3169130263155609_7514841459699383389_o.jpg?_nc_cat=100&amp;_nc_sid=110474&amp;_nc_ohc=b2ur-L_GUA4AX_uRRa7&amp;_nc_ht=scontent.ffln4-1.fna&amp;_nc_tp=6&amp;oh=743e12e8b95bb9815532d071b28b7a47&amp;oe=5F13928D\" class=\"_4-eo _2t9n\" data-render-location=\"permalink\" href=\"https://www.facebook.com/photo.php?fbid=3169130259822276&amp;set=a.159724900762842&amp;type=3&amp;eid=ARBgbaoU95dsrTfFpLL_IKjGdgUqL6qeevvjrqXm8iZhxjUUYt8jKU6sZr3eJEmqBfN2EXFV6_gVaXVr\" style=\"width:500px;\"><div class=\"uiScaledImageContainer _4-ep\" style=\"width:500px;height:281px;\" id=\"u_0_12\"><img class=\"scaledImageFitWidth img\" src=\"https://scontent.ffln4-1.fna.fbcdn.net/v/t1.0-0/p526x296/82957482_3169130263155609_7514841459699383389_o.jpg?_nc_cat=100&amp;_nc_sid=110474&amp;_nc_ohc=b2ur-L_GUA4AX_uRRa7&amp;_nc_ht=scontent.ffln4-1.fna&amp;_nc_tp=6&amp;oh=407f2551636b4039902750f373019613&amp;oe=5F111A46\" data-src=\"https://scontent.ffln4-1.fna.fbcdn.net/v/t1.0-0/p526x296/82957482_3169130263155609_7514841459699383389_o.jpg?_nc_cat=100&amp;_nc_sid=110474&amp;_nc_ohc=b2ur-L_GUA4AX_uRRa7&amp;_nc_ht=scontent.ffln4-1.fna&amp;_nc_tp=6&amp;oh=407f2551636b4039902750f373019613&amp;oe=5F111A46\" alt=\"A imagem pode conter: céu, nuvem, crepúsculo e atividades ao ar livre\" width=\"500\" height=\"282\"></div></a>"
     , "text"     : ""
     , "visible"  : true
     , "width"    : 500
     , "x"        : 627
     , "y"        : 195
     }

   ]

*/



// EOF

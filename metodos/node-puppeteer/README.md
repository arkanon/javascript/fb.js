# Instalação do Puppeteer em container

- http://ghcr.io/puppeteer/puppeteer
- http://pptr.dev/guides/docker
- http://github.com/zoutepopcorn/pup-face
- [How to pass command line arguments to a Node.js program](http://stackoverflow.com/a/24638042)
- http://loginradius.com/blog/engineering/facebook-authentication-using-node-and-passport



```bash

  image=ghcr.io/puppeteer/puppeteer

  puppet()
  {
    local pkgs script credp credf u d args
      pkgs=$1
    script=$(readlink -f $2)
     credp=${3%=*}
     credf=$(readlink -f ${3#*=})
         u=root
         d=/$u
      args=(
        --interactive
        --tty
        --rm
        --volume  $PWD:/host
        --volume  $script:$d/s.js
        --volume  $credf:$d/c.js
        --workdir $d
        --user    $u
      )
    shift 3
    time docker run "${args[@]}" $image bash -c "npm install $pkgs; node $d/s $credp=$d/c $@"
  }

# export PUPPETEER_PRODUCT=chromium
  export PUPPETEER_PRODUCT=chrome
# export PUPPETEER_PRODUCT=firefox

```



```bash
  ((UID>0)) && hash sudo 2>&- && sudo="sudo -E"; echo $sudo

  $sudo apt -y update
  apt list --upgradable
  $sudo apt -y install docker-ce
  $sudo apt -y upgrade

  docker --version

  docker pull $image
  docker images
```



## Container interativo

```bash

  cname=node-puppeteer

  args=(
    --detach
    --interactive
    --tty
  # --init
  # --cap-add    SYS_ADMIN
    --entrypoint bash
    --env        TZ=$(</etc/timezone)
    --volume     $PWD:/host
    --volume     $ADM_DIR/.node:/creds.js
    --volume     script.js:/script.js
  # --env        PUPPETEER_PRODUCT=$PUPPETEER_PRODUCT
  # --workdir    /host
    --user       root
    --hostname   $cname
    --name       $cname
    $image
  )
  declare -p args | tr ' ' '\n'

  sudo rm -r node_modules package.json package-lock.json fb.png
  docker rm -f $cname

  docker run "${args[@]}"

  docker ps -a

  time docker exec -ti $cname npm install npm puppeteer minimist
       docker exec -ti $cname bash
  time docker exec -ti $cname node script --creds=/creds # arquivo com extensão .js mas referido sem ela
  viewnior fb.png

```



## Container não interativo

```bash
  puppet 'puppeteer minimist' script.js --creds=$ADM_DIR/.node
  # 0m34,750s
  viewnior fb.png
```



☐
